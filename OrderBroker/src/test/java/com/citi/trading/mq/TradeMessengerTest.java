package com.citi.trading.mq;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.citi.trading.Trade;
import com.citi.trading.mq.TradeFactory;
import com.citi.trading.mq.TradeMessenger;

/**
Unit tests for the {@link TradeMessenger}.

@author Will Provost
*/
public class TradeMessengerTest
{
    // This is a little fragile -- should compare XML node set
    public static final String TEST_REQUEST_XML = "<trade><buy>false</buy><id>0</id><price>100.0</price><size>1000</size><stock>GE</stock><whenAsDate>2014-07-29T12:00:11.346-04:00</whenAsDate></trade>";
    public static final String TEST_RESPONSE_XML = "<trade><buy>false</buy><id>0</id><price>100.0</price><result>FILLED</result><size>1000</size><stock>GE</stock><whenAsDate>2014-07-29T12:00:11.346-04:00</whenAsDate></trade>";
    
    /**
    Tests production of appropriate XML representation of a trade.
    */
    @Test
    public void testTradeRequestToXML ()
        throws Exception
    {
        Trade trade = TradeFactory.createTestSale ();
        String XML = TradeMessenger.tradeToXML (trade);
        String canonical = 
            XML.replaceAll ("<\\?xml.*\\?>", "")
               .replaceAll ("<whenAsDate>.*</whenAsDate>", "<whenAsDate/>");
        String expectedCanonical = TEST_REQUEST_XML
            .replaceAll ("<whenAsDate>.*</whenAsDate>", "<whenAsDate/>");
        assertEquals (expectedCanonical, canonical);
    }
    
    /**
    Tests production of appropriate XML representation of a trade.
    */
    @Test
    public void testTradeResponseToXML ()
        throws Exception
    {
        Trade trade = TradeFactory.createTestSaleSuccess ();
        String XML = TradeMessenger.tradeToXML (trade);
        String canonical = 
            XML.replaceAll ("<\\?xml.*\\?>", "")
               .replaceAll ("<whenAsDate>.*</whenAsDate>", "<whenAsDate/>");
        String expectedCanonical = TEST_RESPONSE_XML
            .replaceAll ("<whenAsDate>.*</whenAsDate>", "<whenAsDate/>");
        assertEquals (expectedCanonical, canonical);
    }
    
    /**
    Tests parsing of an XML representation of a trade.
    */
    @Test
    public void testTradeFromXML ()
        throws Exception
    {
        Trade trade = TradeMessenger.tradeFromXML (TEST_RESPONSE_XML);
        TradeFactory.assertEqualsTestSaleSuccess (trade);
    }
}
